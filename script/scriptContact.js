let footerContentArray1, footerContentArray2, footerElt, liElt1, aElt, ulElt1, liElt2, ulElt2, spanElt

footerContentArray1 = ['Mentions légales', 'Politique de confidentialité', 'Politique de cookies', 'Contactez-nous'];
footLinksArray = ['mentionslegales.html', 'confidentialite.html', 'cookies.html', 'contact.html']
footerElt = document.createElement('footer');
footerElt.id = 'footerContent';
liElt1 = document.createElement('li');
liElt1.id = 'liElt1';

spanElt = document.createElement('span');
spanElt.id = 'spanElt';

footerElt.appendChild(liElt1);
let i = 0;
footerContentArray1.forEach(function (line) {
    let aElt = document.createElement('a');
    let ulElt = document.createElement('ul');
    aElt.href = footLinksArray[i];
    aElt.classList = 'aElt';
    ulElt.classList = 'ulElt';
    ulElt.textContent = line;
    aElt.appendChild(ulElt);
    liElt1.appendChild(aElt);
    i++;
})

spanElt.id = 'infoSite';
spanElt.textContent = "Site internet fictif créé dans le cadre d'une formation";
footerElt.appendChild(spanElt);

bodyElt.appendChild(footerElt);
