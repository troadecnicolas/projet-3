let footerContentArray1, footerContentArray2, footerElt, liElt1, aElt, ulElt1, liElt2, ulElt2, spanElt

footerContentArray1 = ['Mentions légales', 'Politique de confidentialité', 'Supprimer vos données', 'Politique de cookies', 'Contactez-nous'];
footerContentArray2 = ['Comment nous contacter ?', 'Téléphone : 01 02 03 04 05', 'Email : contact@filmsdepleinair.org', 'Adresse : 1 rue de la Paix, Gotham City'];
footLinksArray = ['mentionslegales.html', 'confidentialite.html', 'supprimerdonnees.html', 'cookies.html', 'contact.html'];
footerElt = document.createElement('footer');
footerElt.id = 'footerContent';

liElt1 = document.createElement('li');
liElt2 = document.createElement('li');
liElt1.id = 'liElt1';
liElt2.id = 'liElt2';

spanElt = document.createElement('span');
spanElt.id = 'spanElt';

footerElt.appendChild(liElt1);
footerElt.appendChild(liElt2);

let i = 0;
footerContentArray1.forEach(function (line) {
    let aElt = document.createElement('a');
    let ulElt = document.createElement('ul');
    aElt.href = footLinksArray[i];
    aElt.classList = 'aElt';
    ulElt.classList = 'ulElt';
    ulElt.textContent = line;
    aElt.appendChild(ulElt);
    liElt1.appendChild(aElt);
    i++;
})

footerContentArray2.forEach(function (line) {
    let ulElt = document.createElement('ul');
    ulElt.textContent = line;
    ulElt.classList = 'ulElt';
    liElt2.appendChild(ulElt);
})
spanElt.id = 'infoSite';
spanElt.textContent = "Site internet fictif créé dans le cadre d'une formation";
footerElt.appendChild(spanElt);
bodyElt.appendChild(footerElt);